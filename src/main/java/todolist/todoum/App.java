package todolist.todoum;

import java.util.List;
import java.util.Optional;

import com.dieselpoint.norm.Database;

import java.sql.Date;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import todolist.todoum.List_to_do;
import todolist.todoum.SystemInfo;


/**
 * JavaFX App
 */

public class App extends Application {
	
	public String str="";
	public TextField text = new TextField();
	public Button btn2 = new Button("X");
	public ListView<List_to_do> listview = new ListView();
	public Database db = new Database();
	public Button btn = new Button("✔");
	public DatePicker datepick = new DatePicker();
	public CheckBox check = new CheckBox();
	public List_to_do todo = new  List_to_do();
	public Font font = new Font(10);
	
	public Insets getInsets(int a, int b, int c, int d) {
		 return new Insets(a,b,c,d);
		}
	
	protected Background bg = new Background(new BackgroundFill(
            Color.color(
                    Color.DEEPPINK.getRed(),
                    Color.HOTPINK.getGreen(), 
                    Color.LIGHTPINK.getBlue(), 0.4d),
                new CornerRadii(5), 
                null));
	
	protected Background toi = new Background(new BackgroundFill(
            Color.color(
                    Color.BLACK.getRed(),
                    Color.DARKBLUE.getGreen(), 
                    Color.DARKCYAN.getBlue(), 0.4d),
                new CornerRadii(5), 
                null));
	
	protected Background noir = new Background(new BackgroundFill(
            Color.color(
                    Color.BLACK.getRed(),
                    Color.BLACK.getGreen(), 
                    Color.BLACK.getBlue(), 0.4d),
                new CornerRadii(5), 
                null));

    @Override
    public void start(Stage stage) {
    	
    	db.setJdbcUrl("jdbc:mysql://localhost:3306/to_do_list?useSSL=false");
    	db.setUser("root");
    	db.setPassword("");

    	
        var javaVersion = SystemInfo.javaVersion();
        var javafxVersion = SystemInfo.javafxVersion();
        
        var font = new Font(20);
        
        
        List<List_to_do> myList = db.orderBy("idList").results(List_to_do.class);
        var root = new BorderPane();
        var box = new HBox();
        
        btn.setFont(font);
        box.getChildren().add(text);
        box.getChildren().add(datepick);
        box.getChildren().add(btn);
        box.getChildren().add(check);
        box.getChildren().add(btn2);
        text.setPadding(new Insets(15));
        datepick.setPadding(new Insets(10));
        check.setPadding(new Insets(10));
        btn2.setPadding(new Insets(10));
        box.setMargin(text, getInsets(10,10,10,10));
        box.setMargin(datepick, getInsets(10,10,10,10));
        box.setMargin(btn, getInsets(10,10,10,10));
        box.setMargin(check, getInsets(15,10,10,10));
        box.setMargin(btn2, getInsets(10,10,10,10));
        root.setCenter(listview);
        root.setBottom(box);
        box.setPadding(new Insets(5));
        btn.setShape(new Circle(30));
        btn.setPrefSize(50,50);
        btn2.setPrefSize(50,50);
        btn2.setShape(new Circle(30));
        btn.setOnAction(new HandleClick());
        box.setBackground(bg);
        stage.setResizable(false);
        text.setBackground(toi);
        btn.setBackground(toi);
        btn2.setBackground(toi);
        datepick.setBackground(toi);
        check.setBackground(toi);
        listview.setBackground(noir);
        text.setStyle("-fx-text-fill: #00BFFF");
        btn2.setTextFill(Color.RED);
        btn2.setFont(font);
        
        
        var scene = new Scene(root, 600, 480, Color.LIGHTPINK);
        stage.setScene(scene);
        stage.show();
        
        
        
        
        listview.setItems(FXCollections.observableArrayList(myList));
        
        listview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<List_to_do>() {
			@Override
			public void changed(ObservableValue<? extends List_to_do> observable, List_to_do oldValue,List_to_do newValue) {
				// TODO Auto-generated method stub
				check.setSelected(newValue.fait_ou_non);
				check.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						// TODO Auto-generated method stub
						newValue.fait_ou_non = check.isSelected();
						listview.refresh();
						db.update(newValue);
					}
					
				});
				btn2.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						// TODO Auto-generated method stub
						Alert alert = new Alert(AlertType.CONFIRMATION);
				        alert.setTitle("Confirmation Dialog");
				        alert.setHeaderText("Look, a Confirmation Dialog");
				        alert.setContentText("Are you ok with this?");

				        Optional<ButtonType> result = alert.showAndWait();
				        if (result.get() == ButtonType.OK){
				            db.delete(newValue);
				            listview.getItems().remove(newValue);
				        } else {
				            // ... user chose CANCEL or closed the dialog
				        }
						
						listview.refresh();
					}
					
					
				});
			}
        	
        }
       );
        
        
    }
    
    class HandleClick implements EventHandler<ActionEvent>{
    	@Override
        public void handle(ActionEvent event) {
    		 List_to_do todo = new  List_to_do();
    		 todo.nom = text.getText();
    		 todo.date_butoire = Date.valueOf(datepick.getValue());
    		db.insert(todo);
    		listview.getItems().add(todo);
    		text.setText("");
    		
    	}
    	
    }

    public static void main(String[] args) {
        launch();
    }

}
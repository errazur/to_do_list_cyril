package todolist.todoum;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Table(name="to_do")
public class List_to_do {
	
	@Id
	@GeneratedValue
	public int idList;
	public String nom;
	public Date date_butoire;
	public boolean fait_ou_non;
	
	public String toString() {
		return String.format("%d / %s / %s / %s", idList, nom, date_butoire, fait_ou_non? "fait":"pas fait");
	}
	
}

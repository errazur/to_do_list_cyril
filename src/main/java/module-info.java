module todolist.todoum {
    requires javafx.controls;
	requires norm;
	requires java.sql;
	requires persistence.api;
	requires javafx.base;
	requires javafx.graphics;
    exports todolist.todoum;
}